package fr.mobarzik.pcodecompiler.Token;

public enum TokenType {
    BlockComment,

    WhiteSpace,

    Tab,

    NewLine,

    Assign,

    StringDouble,

    StringSingle,

    LineComment,

    CloseBrace,

    OpenBrace,

    OpeningCurlyBrace,

    ClosingCurlyBrace,

    DoubleConstant,

    IntConstant,

    Plus,

    Minus,

    Multiply,

    Divide,

    Point,

    EqualEqual,

    Declare,

    Equal,

    ExclameEqual,

    Greater,

    Less,

    Static,

    Public,

    Private,

    Int,

    Double,

    Void,

    False,

    True,

    Null,

    Return,

    New,

    Class,

    If,

    While,

    Else,

    Semicolon,

    Comma,

    Identifier;

    /**
     * Determines if this token is auxiliary
     *
     * @return {@code true} if token is auxiliary, {@code false} otherwise
     */
    public boolean isAuxiliary() {
        return this == BlockComment || this == LineComment || this == WhiteSpace || this == Tab || this == NewLine ;
    }
}

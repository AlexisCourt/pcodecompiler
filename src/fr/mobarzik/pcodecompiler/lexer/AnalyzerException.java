package fr.mobarzik.pcodecompiler.lexer;

public class AnalyzerException extends Exception {
    public AnalyzerException(String s, int position) {
        System.err.println(s);
    }
}

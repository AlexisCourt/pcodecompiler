package fr.mobarzik.pcodecompiler.lexer;

import fr.mobarzik.pcodecompiler.Token.Token;
import fr.mobarzik.pcodecompiler.Token.TokenType;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lexer {
    private Map<TokenType, String> regexMap;
    private List<Token> result;
    private String file;


    public Lexer(String file){
        regexMap = new TreeMap<TokenType, String>();
        this.file = file;
        fillRegexMap();
        result = new ArrayList<Token>();
    }

    private void fillRegexMap() {

        this.regexMap.put(TokenType.BlockComment, "(/\\*.*?\\*/).*");
        this.regexMap.put(TokenType.LineComment, "(//(.*?)[\r$]?\n).*");
        this.regexMap.put(TokenType.WhiteSpace, "( ).*");
        this.regexMap.put(TokenType.OpenBrace, "(\\().*");
        this.regexMap.put(TokenType.CloseBrace, "(\\)).*");
        this.regexMap.put(TokenType.Semicolon, "(;).*");
        this.regexMap.put(TokenType.Comma, "(,).*");
        this.regexMap.put(TokenType.OpeningCurlyBrace, "(\\{).*");
        this.regexMap.put(TokenType.ClosingCurlyBrace, "(\\}).*");
        this.regexMap.put(TokenType.DoubleConstant, "\\b(\\d{1,9}\\.\\d{1,32})\\b.*");
        this.regexMap.put(TokenType.IntConstant, "\\b(\\d{1,9})\\b.*");
        this.regexMap.put(TokenType.Void, "\\b(void)\\b.*");
        this.regexMap.put(TokenType.Int, "\\b(entier)\\b.*");
        this.regexMap.put(TokenType.Double, "\\b(entier|reel)\\b.*");
        this.regexMap.put(TokenType.Tab, "(\\t).*");
        this.regexMap.put(TokenType.NewLine, "(\\n).*");
        this.regexMap.put(TokenType.False, "\\b(FAUX)\\b.*");
        this.regexMap.put(TokenType.True, "\\b(VRAI)\\b.*");
        this.regexMap.put(TokenType.Null, "\\b(nil)\\b.*");
        this.regexMap.put(TokenType.Return, "\\b(retourne)\\b.*");
        this.regexMap.put(TokenType.If, "\\b(si)\\b.*");
        this.regexMap.put(TokenType.Else, "\\b(sinon)\\b.*");
        this.regexMap.put(TokenType.While, "\\b(tantque)\\b.*");
        this.regexMap.put(TokenType.Point, "(\\.).*");
        this.regexMap.put(TokenType.Plus, "(\\+{1}).*");
        this.regexMap.put(TokenType.Minus, "(\\-{1}).*");
        this.regexMap.put(TokenType.Multiply, "(\\*).*");
        this.regexMap.put(TokenType.Divide, "(/).*");
        this.regexMap.put(TokenType.EqualEqual, "(==).*");
        this.regexMap.put(TokenType.Declare, "(:).*");
        this.regexMap.put(TokenType.Assign, "(<-).*");
        this.regexMap.put(TokenType.StringDouble, "(\".*\").*");
        this.regexMap.put(TokenType.StringSingle, "('.*').*");
        this.regexMap.put(TokenType.Equal, "(=).*");
        this.regexMap.put(TokenType.ExclameEqual, "(\\!=).*");
        this.regexMap.put(TokenType.Greater, "(>).*");
        this.regexMap.put(TokenType.Less, "(<).*");
        this.regexMap.put(TokenType.Identifier, "\\b([a-zA-Z]{1}[0-9a-zA-Z_]{0,31})\\b.*");
    }

    public void tokenize(String source) throws AnalyzerException{

        int position = 0;
        Token token = null;
        do {
            token = separateToken(source, position);
            if (token != null) {
                System.out.println(token.getTokenString());
                position = token.getEnd();
                result.add(token);
            }
        } while (token != null && position != source.length());
        if (position != source.length()) {
            throw new AnalyzerException("Lexical error at position # "+ position , position);

        }

    }

    /**
     * Returns a sequence of tokens
     *
     * @return list of tokens
     */
    public List<Token> getTokens() {
        return result;
    }

    /**
     * Returns a sequence of tokens without types {@code BlockComment},
     * {@code LineComment} , {@code NewLine}, {@code Tab}, {@code WhiteSpace}
     *
     * @return list of tokens
     */
    public List<Token> getFilteredTokens() {
        List<Token> filteredResult = new ArrayList<Token>();
        for (Token t : this.result) {
            if (!t.getTokenType().isAuxiliary()) {
                filteredResult.add(t);
            }
        }
        return filteredResult;
    }

    private Token separateToken(String source, int fromIndex) {
        if (fromIndex < 0 || fromIndex >= source.length()) {
            throw new IllegalArgumentException("Illegal index in the input stream!");
        }
        for (TokenType tokenType : TokenType.values()) {
            Pattern p = Pattern.compile(".{" + fromIndex + "}" + regexMap.get(tokenType),
                    Pattern.DOTALL);
            Matcher m = p.matcher(source);
            if (m.matches()) {
                String lexema = m.group(1);
                return new Token(fromIndex, fromIndex + lexema.length(), lexema, tokenType);
            }
        }

        return null;
    }



}
